package com.corporation.pruebaupax.usecases

import android.content.Context
import android.widget.Toast
import java.io.File
import java.io.FileOutputStream
import java.net.URL

interface ThreadPresenter {
    //Interface to show a toast on activity who called this
    val context: Context
    fun toast(message: String) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

    }

    fun download(link: String, path: String) {
        URL(link).openStream().use { input ->
            FileOutputStream(File(path)).use { output ->
                input.copyTo(output)
            }
        }
    }
}